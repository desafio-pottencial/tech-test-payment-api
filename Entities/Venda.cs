using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public List<Produto> Produtos { get; set; }
        public Vendedor Vendedor { get; set; }
        public DateTime DataVenda { get; set; }
        public Status Status { get; set; }
        public double ValorTotalDaCompra { get; set; } 

        public Venda(int id, List<Produto> produtos, Vendedor vendedor, DateTime dataVenda, Status status)
        {
            Id = id;
            Produtos = produtos;
            Vendedor = vendedor;
            DataVenda = dataVenda;
            Status = status;
            ValorTotalDaCompra = CalcularValorDaCompra();
        } 

        public double CalcularValorDaCompra()
        {   
            double valorTotalDaCompra = 0;
            for (int i = 0; i < Produtos.Count; i++)
            {
                Produtos[i].PrecoTotal = Produtos[i].CalcularValorTotalDoProduto();
                valorTotalDaCompra += Produtos[i].PrecoTotal; 
            }
            return valorTotalDaCompra;
        }

        public bool AtualizarStatus(Status status)
        {
            this.Status = status;
            return true;
        }

    }
}