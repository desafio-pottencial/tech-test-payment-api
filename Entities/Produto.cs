using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Produto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public double Preco { get; set; }
        public int Quantidade { get; set; }
        public double PrecoTotal { get; set; }

        public Produto(int id, string nome, double preco, int quantidade)
        {
            Id = id;
            Nome = nome;
            Preco = preco;
            Quantidade = quantidade;
            CalcularValorTotalDoProduto();
        }

        public double CalcularValorTotalDoProduto()
        {
            PrecoTotal = Preco * Quantidade;
            return PrecoTotal;
        }
    }
}