namespace tech_test_payment_api.Entities
{
    public enum Status
    {
        AguardandoPagamento,
        PagamentoAprovado, 
        EnviadoParaTransportadora, 
        Entregue, 
        Cancelada
    }
}