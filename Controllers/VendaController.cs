using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Context;


namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        [HttpPost]
        [Route("nova-venda")]
        public IActionResult NovaVenda(Venda venda)
        {
            ContextVendas.AutoId++;
            venda.Id = ContextVendas.AutoId;
            venda.ValorTotalDaCompra = venda.CalcularValorDaCompra();
            ContextVendas.Vendas.Add(venda);
            return Ok(venda);
        }

        [HttpGet()]
        [Route("{id:int}")]
        public IActionResult ObterPorId(int id)
        {
            Venda venda = null;
            /*for (int i = 0; i < ContextVendas.Vendas.Count; i++)
            {
                if(ContextVendas.Vendas[i].Id == id)
                {
                    venda = ContextVendas.Vendas[i];
                    break;
                }
            }*/
            venda = ContextVendas.Vendas.FirstOrDefault(x => x.Id == id);
            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

        [HttpPut()]
        [Route("atualizar-status/{id:int}")]
        public IActionResult AtualizarStatus(int id, Status status)
        {
            Venda venda = ContextVendas.Vendas.FirstOrDefault(x => x.Id == id);
            if (venda == null)
                return NotFound();

            venda.Status = status;
            return Ok(venda);
        }
    }
}